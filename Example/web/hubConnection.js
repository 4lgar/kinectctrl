  /* Connection to SignalR Event Hub --------------------------------*/

  $.connection.hub.url = "http://127.0.0.1:8088/signalr";
  var eventHub = $.connection.EventHub;

  $.connection.hub.start().done(function(){

    console.log("Connected to Event Hub :)");

  }).fail(function(){

    console.log("OwCrap :(");

  });

  /* JS Debouncer (from https://davidwalsh.name/javascript-debounce-function) */

  // Returns a function, that, as long as it continues to be invoked, will not
  // be triggered. The function will be called after it stops being called for
  // N milliseconds. If `immediate` is passed, trigger the function on the
  // leading edge, instead of the trailing.
  function debounce(func, wait, immediate) {
    var timeout;
    return function() {
      var context = this, args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  };

  var defaultDebounceTime = 90;

  /* Client side callback -------------------------------------------*/

  eventHub.client.Left = debounce(function() {
    
    console.log("<<< Left");

  }, defaultDebounceTime);

  eventHub.client.Right = debounce(function() {
    
    console.log(">>> Right");

  }, defaultDebounceTime);

  eventHub.client.Enter = debounce(function(){

    console.log("Enter");

  }, defaultDebounceTime);

  eventHub.client.Hello = debounce(function(){
    
    console.log("Hello :)");

  }, defaultDebounceTime);

  eventHub.client.Goodbye = debounce(function(){
    
    console.log("Goodbye!");

  }, defaultDebounceTime);

  eventHub.client.PeopleEnter = function(){
    
    console.log("People Enter");

  };  

  eventHub.client.PeopleLeave = function(){
    
    console.log("People Leave");

  };  