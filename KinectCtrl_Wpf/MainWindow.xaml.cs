﻿using KinectCtrl_Wpf;
using Microsoft.Kinect;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Hosting;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

[assembly: OwinStartup(typeof(MainWindow.Startup))]
namespace KinectCtrl_Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Private fields

        private KinectGesture GestureRecognizer;
        private List<GestureEvent> GestureToRecognize;

        public IDisposable SignalR { get; set; }
        const string ServerURI = "http://127.0.0.1:8088";

        #endregion

        #region Gesture Definition

        public GestureEvent OnLeft = new GestureEvent(@"../../../Gestures/SwipeLeft.gbd")
        {
            Name = "SwipeLeft",
            Threshold = 0.4f,
            CallBack = EventHub.Left
        };

        public GestureEvent OnRight = new GestureEvent(@"../../../Gestures/SwipeRight.gbd")
        {
            Name = "SwipeRight",
            Threshold = 0.4f,
            CallBack = EventHub.Right
        };

        public GestureEvent OnEnter = new GestureEvent(@"../../../Gestures/Enter.gbd")
        {
            Name = "Enter",
            Threshold = 0.4f,
            CallBack = EventHub.Enter
        };

        #endregion

        #region Signalr Srv methods

        public class Startup
        {
            public void Configuration(IAppBuilder app)
            {
                app.UseCors(CorsOptions.AllowAll);
                app.MapSignalR();
            }
        }

        private void StartServer()
        {
            SignalR = WebApp.Start(ServerURI);
        }

        #endregion

        public MainWindow()
        {
            InitializeComponent();

            Task.Run(() => StartServer());

            GestureRecognizer = new KinectGesture();
            GestureToRecognize = new List<GestureEvent>();

            Loaded += OnLoaded;
            Closing += OnClosing;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            GestureToRecognize.Add(OnLeft);
            GestureToRecognize.Add(OnRight);
            GestureToRecognize.Add(OnEnter);


            GestureRecognizer.Init(GestureToRecognize, CameraView, EventHub.PeopleEnter, EventHub.PeopleLeave);
            GestureRecognizer.Start();
        }

        private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            GestureRecognizer.Stop();
        }
    }

}
