﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectCtrl_Wpf
{
    [HubName("EventHub")]
    public class EventHub : Hub
    {
        public override Task OnConnected()
        {
            return base.OnConnected();
        }

        public static void Left()
        {
            var Context = GlobalHost.ConnectionManager.GetHubContext<EventHub>();
            Context.Clients.All.Left();
        }

        public static void Right()
        {
            var Context = GlobalHost.ConnectionManager.GetHubContext<EventHub>();
            Context.Clients.All.Right();
        }

        public static void Enter()
        {
            var Context = GlobalHost.ConnectionManager.GetHubContext<EventHub>();
            Context.Clients.All.Enter();
        }

        public static void Hello()
        {
            var Context = GlobalHost.ConnectionManager.GetHubContext<EventHub>();
            Context.Clients.All.Hello();
        }

        public static void Goodbye()
        {
            var Context = GlobalHost.ConnectionManager.GetHubContext<EventHub>();
            Context.Clients.All.Goodbye();
        }

        public static void PeopleEnter()
        {
            var Context = GlobalHost.ConnectionManager.GetHubContext<EventHub>();
            Context.Clients.All.PeopleEnter();
        }

        public static void PeopleLeave()
        {
            var Context = GlobalHost.ConnectionManager.GetHubContext<EventHub>();
            Context.Clients.All.PeopleLeave();
        }

    }
}
