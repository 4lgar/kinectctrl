﻿using Microsoft.Kinect;
using Microsoft.Kinect.VisualGestureBuilder;
using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Linq;

/* Inspired from buu0528/KinectV2Gesture | GitHub*/

namespace KinectCtrl_Wpf
{
    public class GestureEvent
    {
        public VisualGestureBuilderDatabase GestureToDetect;

        public String Name { get; set; }
        public float Threshold { get; set; }
        public Action CallBack { get; set; }

        public GestureEvent(string gbdFile)
        {
            GestureToDetect = new VisualGestureBuilderDatabase(gbdFile);
        }


        public static GestureEvent Find(List<GestureEvent> list, Gesture gestureToFind)
        {
            foreach(GestureEvent ge in list)
                if (ge.GestureToDetect.AvailableGestures.ToList().Find(g => g.Name == gestureToFind.Name) != null)
                    return ge;

            return null;
        }
    }

    public class GestureBuilderFrame
    {
        public int BodyId { get; set; }
        public VisualGestureBuilderFrameSource GestureFrameSource { get; set; }
        public VisualGestureBuilderFrameReader GestureFrameReader { get; set; }

        public GestureBuilderFrame(int bodyId, KinectSensor sensor, List<GestureEvent> registredEvent, EventHandler<VisualGestureBuilderFrameArrivedEventArgs> eventHandler)
        {
            GestureFrameSource = new VisualGestureBuilderFrameSource(sensor, 0);
            GestureFrameReader = GestureFrameSource.OpenReader();

            GestureFrameReader.IsPaused = true;
            GestureFrameReader.FrameArrived += eventHandler;

            BodyId = bodyId;

            foreach (GestureEvent ge in registredEvent)
                foreach (var g in ge.GestureToDetect.AvailableGestures)
                    if (g.GestureType == GestureType.Discrete)
                        GestureFrameSource.AddGesture(g);
        }

        ~GestureBuilderFrame()
        {
            GestureFrameReader.Dispose();
            GestureFrameSource.Dispose();
        }
    }

    public class KinectGesture
    {
        // Kinect
        private KinectSensor kinect;

        // Gesture Builder
        private List<GestureBuilderFrame> GestureBuilderList;
        /*
        private VisualGestureBuilderFrameSource GestureFrameSource;
        private VisualGestureBuilderFrameReader GestureFrameReader;
        */

        // Camera view
        private byte[] ColorBuffer;
        private FrameDescription ColorFrameDescription;
        private MultiSourceFrameReader MultiFrameReader;
        private Image CameraView;

        // Detected Bodies
        private Body[] Bodies;
        bool hadPeople = false;
        Action OnPeopleEnter;
        Action OnPeopleLeave;

        private List<GestureEvent> RegistredEvent;

        public KinectGesture()
        {
            kinect = KinectSensor.GetDefault();
            //GestureFrameSource = new VisualGestureBuilderFrameSource(kinect, 0);
            GestureBuilderList = new List<GestureBuilderFrame>();
        }

        public void Init(List<GestureEvent> eventToRegister, Image cameraView, Action onPeopleEnter, Action onPeopleLeave)
        {
            CameraView = cameraView;


            ColorFrameDescription = kinect.ColorFrameSource.CreateFrameDescription(ColorImageFormat.Bgra);
            CameraView.Source = new WriteableBitmap(ColorFrameDescription.Width, ColorFrameDescription.Height, 96, 96, PixelFormats.Bgra32, null);


            Bodies = new Body[kinect.BodyFrameSource.BodyCount];

            OnPeopleEnter = onPeopleEnter;
            OnPeopleLeave = onPeopleLeave;

            RegistredEvent = eventToRegister;
        }


        public void Start()
        {
            kinect.Open();

            /*
            GestureFrameReader = GestureFrameSource.OpenReader();
            GestureFrameReader.IsPaused = true;
            GestureFrameReader.FrameArrived += __OnFrameArrived;
            */

            MultiFrameReader = kinect.OpenMultiSourceFrameReader(FrameSourceTypes.Color | FrameSourceTypes.Body);
            MultiFrameReader.MultiSourceFrameArrived += __OnMultiSourceFrameArrived;
        }

        public void Stop()
        {
            if (kinect != null)
            {
                kinect.Close();
                kinect = null;
            }

            /*

            if (GestureFrameReader != null)
            {
                GestureFrameReader.Dispose();
                GestureFrameReader = null;
            }

            */
        }

        private void __OnMultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            MultiSourceFrame multiFrame = e.FrameReference.AcquireFrame();

            using (var colorFrame = multiFrame.ColorFrameReference.AcquireFrame())
            {
                if (colorFrame == null)
                    return;

                ColorBuffer = new byte[ColorFrameDescription.Width * ColorFrameDescription.Height * ColorFrameDescription.BytesPerPixel];
                colorFrame.CopyConvertedFrameDataToArray(ColorBuffer, ColorImageFormat.Bgra);

                CameraView.Source = BitmapSource.Create(ColorFrameDescription.Width, ColorFrameDescription.Height, 96, 96,
                    PixelFormats.Bgra32, null, ColorBuffer, ColorFrameDescription.Width * (int)ColorFrameDescription.BytesPerPixel);

            }

            using (BodyFrame bodyFrame = multiFrame.BodyFrameReference.AcquireFrame())
            {
                if (bodyFrame != null)
                {
                    bodyFrame.GetAndRefreshBodyData(Bodies);
                    UpdateGestureBuilderFrameList(bodyFrame);
                }
            }

            /*

            if (!GestureFrameSource.IsTrackingIdValid)
            {
                using (BodyFrame bodyFrame = multiFrame.BodyFrameReference.AcquireFrame())
                {
                    if (bodyFrame != null)
                    {
                        bodyFrame.GetAndRefreshBodyData(Bodies);

                        
                        if(GetNbrTrackedBodies() >= 1 && !hadPeople)
                        {
                            hadPeople = true;
                            OnPeopleEnter();
                        }

                        if(GetNbrTrackedBodies() == 0 && hadPeople)
                        {
                            hadPeople = false;
                            OnPeopleLeave();
                        }


                        foreach (var body in Bodies)
                        {
                            if (body.IsTracked)
                            {
                                GestureFrameSource.TrackingId = body.TrackingId;
                                GestureFrameReader.IsPaused = false;
                                break;
                            }
                        }
                    }
                }
            }

            */
        }

        private void __OnFrameArrived(object sender, VisualGestureBuilderFrameArrivedEventArgs e)
        {
            using (VisualGestureBuilderFrame gestureFrame = e.FrameReference.AcquireFrame())
            {
                if (gestureFrame == null || gestureFrame.DiscreteGestureResults == null)
                    return;

                    foreach (KeyValuePair<Gesture, DiscreteGestureResult> g in gestureFrame.DiscreteGestureResults)
                {
                    if (!g.Value.Detected)
                        continue;

                    GestureEvent eventToDeal = GestureEvent.Find(RegistredEvent, g.Key);

                    if (eventToDeal == null)
                        continue;

                    if (g.Value.Confidence >= eventToDeal.Threshold)
                        eventToDeal.CallBack();
                }
            }
        }

        /* Because TablarSoft: https://elbruno.com/2015/01/02/kinectsdk-beware-of-the-array-of-bodies-not-all-bodies-are-tracked-by-defaultk-in-kinectv2/ */
        private int GetNbrTrackedBodies()
        {
            return Bodies.ToList().FindAll(b => b.IsTracked == true).Count;
        }

        private void UpdateGestureBuilderFrameList(BodyFrame bodyFrame)
        {
            GestureBuilderFrame tmpGestureBuilderFrame;
            VisualGestureBuilderFrameSource tmpGestureFrameSource;
            VisualGestureBuilderFrameReader tmpGestureFrameReader;


            if (GetNbrTrackedBodies() >= 1 && !hadPeople)
            {
                hadPeople = true;
                OnPeopleEnter();
            }

            if (GetNbrTrackedBodies() == 0 && hadPeople)
            {
                hadPeople = false;
                OnPeopleLeave();
            }


            for (int i = 0; i < Bodies.Length; i++)
            {
                tmpGestureBuilderFrame = GestureBuilderList.Find(g => g.BodyId == i);

                if (Bodies[i].IsTracked)
                {

                    if (tmpGestureBuilderFrame == null)
                    {
                        tmpGestureBuilderFrame = new GestureBuilderFrame(i, kinect, RegistredEvent, __OnFrameArrived);
                        GestureBuilderList.Add(tmpGestureBuilderFrame);
                    }

                    tmpGestureFrameSource = tmpGestureBuilderFrame.GestureFrameSource;
                    tmpGestureFrameReader = tmpGestureBuilderFrame.GestureFrameReader;

                    if (!tmpGestureFrameSource.IsTrackingIdValid)
                    {
                        tmpGestureFrameSource.TrackingId = Bodies[i].TrackingId;
                        tmpGestureFrameReader.IsPaused = false;
                    }
                }
                else
                {
                    if(tmpGestureBuilderFrame != null)
                    {
                        GestureBuilderList.Remove(tmpGestureBuilderFrame);
                    }
                }
            }
        }
    }
}
