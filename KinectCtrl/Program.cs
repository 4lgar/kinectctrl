﻿using KinectCtrl;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Hosting;
using Owin;
using System;
using System.Collections.Generic;

[assembly: OwinStartup(typeof(Program.Startup))]
namespace KinectCtrl
{
    public class Program
    {
        static IDisposable SignalR;

        public class Startup
        {
            public void Configuration(IAppBuilder app)
            {
                app.UseCors(CorsOptions.AllowAll);
                app.MapSignalR();
            }
        }

        static void Main(string[] args)
        {


            /* Init ---------------------------------------------- */

            string url;

            if (args.Length != 1)
                url = "http://127.0.0.1:8088";
            else
                url = args[0];


            SignalR = WebApp.Start(url);

            Console.WriteLine("Server listening on " + url);
            Console.WriteLine("Key you can use: Left Arrow, Right Arrow, Enter, Space, Escape.");
            Console.WriteLine("Use x key to exit.");

            bool run = true;
            ConsoleKeyInfo k;


            /* Key action ---------------------------------------- */

            while(run)
            {
                k = Console.ReadKey(true);

                switch (k.Key)
                {
                    case ConsoleKey.X:
                        run = false;
                        break;
                    case ConsoleKey.LeftArrow:
                        EventHub.Left();
                        break;
                    case ConsoleKey.RightArrow:
                        EventHub.Right();
                        break;
                    case ConsoleKey.Enter:
                        EventHub.Enter();
                        break;
                    case ConsoleKey.Spacebar:
                        EventHub.Hello();
                        break;
                    case ConsoleKey.Escape:
                        EventHub.Goodbye();
                        break;
                    default:
                        break;
                }
            }


        }

    }
}
